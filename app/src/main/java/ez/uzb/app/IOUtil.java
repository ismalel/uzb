
package ez.uzb.app;

import java.io.Closeable;
import java.io.IOException;

public class IOUtil {


    public static void forceClose(Closeable closeable) {
        if (closeable == null)
            return;
        try {
            closeable.close();
        } catch (IOException e) {
            // ignore
        }
    }

}
